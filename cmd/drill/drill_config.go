package main

import (
	"bytes"
	"context"
	"drill/pkg/output"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"
	"text/template"

	"github.com/fatih/color"
)

// FlowType is used as an enum type to indicate the type of the flow
type FlowType string

const (
	// CLEAN FlowType indicates the clean flow type
	CLEAN FlowType = "clean"

	// BUILD FlowType indicates the build flow type
	BUILD FlowType = "build"

	// WATCH FlowType indicates the watch flow type
	WATCH FlowType = "watch"
)

// Env stores the environment variables in a map
// which denotes a key value pair
type Env map[string]string

// DrillStep struct contains all the cionfiguration
// of a particular step
type DrillStep struct {
	Name        string   `yaml:"name"`
	Description string   `yaml:"description"`
	Environment Env      `yaml:"environment"`
	WorkDir     string   `yaml:"workDir"`
	Commands    []string `yaml:"commands"`
}

// DrillFlow stuct contains all the metadata configured in a single flow step
type DrillFlow struct {
	Step           string   `yaml:"step"`
	Configurations []string `yaml:"configurations"`
}

// importedSteps struct contains all the steps defined in a config file
type importedSteps struct {
	Steps map[string]*DrillStep `yaml:"steps"`
}

// DrillConfig struct contains all the configuration
// data contained to run the build
type DrillConfig struct {
	Version        string                `yaml:"projectName"`
	WatchMatchers  []string              `yaml:"watchMatchers"`
	Import         []string              `yaml:"import"`
	Metadata       Env                   `yaml:"metadata"`
	Environment    Env                   `yaml:"environment"`
	Configurations map[string]Env        `yaml:"configurations"`
	Steps          map[string]*DrillStep `yaml:"steps"`
	BuildFlow      []DrillFlow           `yaml:"buildFlow"`
	CleanFlow      []DrillFlow           `yaml:"cleanFlow"`
	WatchFlow      []DrillFlow           `yaml:"watchFlow"`

	// evaluated runtime environments
	env         Env
	evalConfigs map[string]Env
}

// RunStep runs a particular step of the pipeline
// TODO: Re-factor this method
func (dc *DrillConfig) RunStep(ctx context.Context, name string, configName string, configuration Env) error {
	// Run the step command here
	step, ok := dc.Steps[name]
	if !ok {
		return fmt.Errorf("No step '%s' found", name)
	}

	env, err := calculateEnv(step.Environment)
	if err != nil {
		return err
	}

	workDir := step.WorkDir
	if workDir == "" {
		workDir, err = os.Getwd()
		if err != nil {
			return err
		}
	}

	stat, err := os.Stat(workDir)
	if err != nil {
		return err
	}

	if !stat.IsDir() {
		return fmt.Errorf("workDir specified ('%s') must be a directory", workDir)
	}

	tag := fmt.Sprintf("%s::%s", name, configName)

	output.Printf("Running step", color.GreenString(tag))
	t := template.New("cmd")

	for _, val := range step.Commands {
		tbs := bytes.NewBuffer(make([]byte, 0))
		x, err := t.Parse(val)
		if err == nil {
			err := x.Execute(tbs, dc.Metadata)
			if err == nil {
				val = tbs.String()
			}
		}
		cmd := exec.CommandContext(ctx, "sh", "-c", val)

		cmd.Stderr = output.NewOutWriter(os.Stderr, true, tag)
		cmd.Stdout = output.NewOutWriter(os.Stdout, false, tag)

		cmd.Dir = workDir

		cmd.Env = append(cmd.Env, os.Environ()...)
		cmd.Env = append(cmd.Env, append(kvEnvVariables(configuration))...)
		cmd.Env = append(cmd.Env, append(kvEnvVariables(env), kvEnvVariables(dc.env)...)...)

		err = cmd.Run()
		if err != nil {
			return err
		}
	}

	return nil
}

// StartFlow starts the build process in the order of the flow
// provided in the configuration file
func (dc *DrillConfig) StartFlow(ctx context.Context, flowType FlowType) error {
	var flow []DrillFlow
	switch flowType {
	case CLEAN:
		flow = dc.CleanFlow
	case BUILD:
		flow = dc.BuildFlow
	case WATCH:
		flow = dc.WatchFlow
	default:
		flow = dc.BuildFlow
	}
	var err error

	// Calculate the common env
	dc.env, err = calculateEnv(dc.Environment)
	if err != nil {
		return err
	}

	dc.evalConfigs = make(map[string]Env)

	for key, val := range dc.Configurations {
		dc.evalConfigs[key], err = calculateEnv(val)
		if err != nil {
			return err
		}
	}
	err = dc.RunFlow(ctx, flow)
	if err != nil {
		return err
	}
	output.Printf("Done flow", color.GreenString(string(flowType)))
	return nil
}

// RunFlow runs the given flow for a given drill config
func (dc *DrillConfig) RunFlow(ctx context.Context, flow []DrillFlow) error {
	for _, val := range flow {
		if len(val.Configurations) == 0 {
			err := dc.RunStep(ctx, val.Step, "default", make(Env))
			if err != nil {
				return err
			}
			continue
		}

		errorChannel := make(chan error)
		wg := &sync.WaitGroup{}
		done := make(chan bool)
		go func() {
			wg.Wait()
			done <- true
		}()
		wg.Add(len(val.Configurations))
		for _, conf := range val.Configurations {

			go func(done *sync.WaitGroup, errorSignal chan error, drillConfig *DrillConfig, config string) {
				defer done.Done()
				env, ok := drillConfig.evalConfigs[config]
				if !ok {
					errorSignal <- errors.New("No such configuration found")
					return
				}
				err := drillConfig.RunStep(ctx, val.Step, config, env)
				if err != nil {
					errorSignal <- err
					return
				}
			}(wg, errorChannel, dc, conf)

		}

		select {
		case err := <-errorChannel:
			return err
		case <-done:
			continue
		}
	}

	return nil
}

func calculateEnv(envRaw Env) (Env, error) {
	envCalc := make(Env)

	for key, value := range envRaw {

		if !strings.HasPrefix(value, "$ ") {
			envCalc[key] = value
			continue
		}
		bs := bytes.NewBuffer(make([]byte, 0))
		errbs := bytes.NewBuffer(make([]byte, 0))
		cmd := exec.Command("sh", "-c", strings.TrimPrefix(value, "$ "))
		cmd.Stdout = bs
		cmd.Stderr = errbs

		err := cmd.Run()
		if err != nil {
			return nil, fmt.Errorf(
				"Error while evaluating the environment command:\n\t%s\nError:\n\t%s\nError Info:\n\t%s",
				value,
				err,
				strings.TrimSpace(errbs.String()),
			)
		}

		envCalc[key] = strings.TrimSpace(bs.String())
	}

	return envCalc, nil
}

func kvEnvVariables(kv Env) []string {
	x := make([]string, 0)

	for key, value := range kv {
		x = append(x, fmt.Sprintf("%s=%s", key, value))
	}

	return x
}
